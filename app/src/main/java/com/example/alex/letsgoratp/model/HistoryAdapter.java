package com.example.alex.letsgoratp.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.repository.TravelRepository;

import java.util.ArrayList;

/**
 * Created by alex on 19/04/17.
 */

public class HistoryAdapter extends BaseAdapter {


    private final Context context;
    private final ArrayList<Travel> travels;

    public HistoryAdapter(Context context, ArrayList<Travel> travels) {
        this.context = context;
        this.travels = travels;
    }

    @Override
    public int getCount() {
        return travels.size();
    }

    @Override
    public Object getItem(int position) {
        return travels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.history_row, null);
        TextView emplacementAllee = (TextView) rowView.findViewById(R.id.emplacementAllee);
        TextView emplacementDestination = (TextView)rowView.findViewById(R.id.emplacementDestination);

        emplacementAllee.setText(travels.get(position).getFrom());
        emplacementDestination.setText(travels.get(position).getTo());
        return rowView;
    }
}