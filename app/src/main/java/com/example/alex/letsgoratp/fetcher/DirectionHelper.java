package com.example.alex.letsgoratp.fetcher;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

/**
 * Created by alex on 17/04/17.
 */

public class DirectionHelper {

    /**
     *
     * @param strAddress
     * @param context
     * @return LatLng
     */
    public static LatLng getLocationFromAddress(String strAddress, Context context)
    {
        //Create coder with Activity context - this
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng latLng = null;
        try {
            //Get latLng from String
            address = coder.getFromLocationName(strAddress,5);

            //check for null
            if (address == null || address.size() == 0) {
                return null;
            }

            //Lets take first possibility from the all possibilities.
            Address location=address.get(0);
            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return latLng;
    }

    /**
     *
     * @param latitude
     * @param longitude
     * @param context
     * @return String Adress location
     */
    public static String getAddressFromLocation(Double latitude, Double longitude, Context context) {
        String resultat = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            resultat = address + " " + city + " " + postalCode + " " + country;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultat;
    }

    /*
    public static void getTravel(String adressDepart, String adressArrive, GoogleApiDirectionFetcher context) {
        RequestQueue queue = Volley.newRequestQueue(context.getContext());
        String url = null;

        try {
            url = "https://maps.googleapis.com/maps/api/directions/json?origin="
                    + URLEncoder.encode(adressDepart, "UTF-8")
                    + "&destination=" + URLEncoder.encode(adressArrive, "UTF-8")
                    +"&key=AIzaSyB2GHe9IElSIh_HjF6VhoJI3wTE85nEvvg&mode=transit";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final GoogleApiDirectionFetcher mContext = (GoogleApiDirectionFetcher) context;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mContext.onRoutingSuccess(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.onRoutingFailed("Erreur : " + error.getMessage());
                }
            }
        );
        SingletonVolley.getInstance(context.getContext()).addToRequestQueue(stringRequest);
    }
    */

    public static void getTravel(String adressDepart, String adressArrive, GoogleApiDirectionFetcher context) {
        DirectionsRoute route = new DirectionsRoute();
        try {
            DirectionsResult routes = DirectionsApi.getDirections(
                    new GeoApiContext().setApiKey("AIzaSyB2GHe9IElSIh_HjF6VhoJI3wTE85nEvvg"),
                    adressDepart,
                    adressArrive
            ).mode(TravelMode.TRANSIT)
                    .language("fr")
                    .await();
            if (routes.routes.length > 0) {
                route = routes.routes[0];
            }
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (route == null) {
            context.onRoutingFailed("Impossible de récupérer l'itinéraire.");

        } else {
            context.onRoutingSuccess(route);
        }
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }
}
