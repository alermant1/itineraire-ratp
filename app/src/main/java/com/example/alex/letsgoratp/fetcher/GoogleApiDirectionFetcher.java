package com.example.alex.letsgoratp.fetcher;

import android.content.Context;
import android.util.Log;

/*
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
*/
import com.example.alex.letsgoratp.fragment.SearchFragment;
import com.example.alex.letsgoratp.model.Travel;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsRoute;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alex on 17/04/17.
 */

public class GoogleApiDirectionFetcher implements Serializable, IGoogleApiDirectionFetcher { // RoutingListener
    private Context context;
    // ex: ville départ
    private LatLng positionInitiale;
    // ex: ville arrivée
    private LatLng positionFinale;
    private IOnGoogleApiDirection onGoogleApiDirectionListener;

    private String emplacementInitiale;
    private String emplacementFinale;

    public Context getContext() {
        return context;
    }

    /**
     * Utilisé après l'initialisation de cette classe,
     * il faudra implémenter l'interface IOnGoogleApiDirection
     * et appeler cette méthode
     * @param onGoogleApiDirectionListener
     */
    public void setOnGoogleApiDirectionListener(IOnGoogleApiDirection onGoogleApiDirectionListener) {
        this.onGoogleApiDirectionListener = onGoogleApiDirectionListener;
    }

    public GoogleApiDirectionFetcher(Context context) {
        this.context = context;
    }

    public LatLng getPositionInitiale() {
        return positionInitiale;
    }

    public void setPositionInitiale(LatLng positionInitiale) {
        this.positionInitiale = positionInitiale;
    }

    public void init(String positionInitiale, String positionFinale) {
        this.emplacementFinale = positionFinale;
        this.emplacementInitiale = positionInitiale;
        this.positionInitiale = DirectionHelper.getLocationFromAddress(positionInitiale, context);
        this.positionFinale = DirectionHelper.getLocationFromAddress(positionFinale, context);
    }

    public void execute() {
        Log.d("execute", "execute");

        DirectionHelper.getTravel(this.emplacementInitiale, this.emplacementFinale, this);
    }

    @Override
    public void onRoutingSuccess(DirectionsRoute route) {
        Travel travel = new Travel();
        travel.setFavorite(false);
        travel.setWork(false);
        travel.setHome(false);
        travel.setBeginDate(new Date());
        travel.setCreateDate(new Date());
        travel.setEndDate(new Date());
        travel.setFrom(emplacementInitiale);
        LatLng posInit = new LatLng(route.legs[0].startLocation.lat, route.legs[0].startLocation.lng);
        travel.setToPos(posInit);
        travel.setFromPos(positionInitiale);
        travel.setTo(emplacementFinale);
        LatLng posFinal = new LatLng(route.legs[0].endLocation.lat, route.legs[0].endLocation.lng);
        travel.setToPos(posFinal);
        travel.setRoute(route);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(posFinal);
        markerOptions.title("Arrivée");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        travel.setMarkerFinale(markerOptions);

        onGoogleApiDirectionListener.onGoogleDirectionSuccess(travel);
    }


    @Override
    public void onRoutingFailed(String erreur) {
        Log.d("onRoutingFailed", erreur);
        onGoogleApiDirectionListener.onGoogleDirectionFailed(erreur);
    }

    /*
    @Override
    public void onRoutingFailure(RouteException e) {
        onGoogleApiDirectionListener.onGoogleDirectionFailed("La demande n'a pas pu être effectué.");
    }
    @Override
    public void onRoutingCancelled() {
        onGoogleApiDirectionListener.onGoogleDirectionFailed("La demande a été annulé.");
    }
    @Override
    public void onRoutingStart() {

    }


    @Override
    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
        LatLng last = null;
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
        for (int z = 0; z < arrayList.size(); z++) {
            for (int y = 0; y < arrayList.get(z).getPoints().size(); y++) {
                LatLng point = arrayList.get(z).getPoints().get(y);
                options.add(point);
                if (z == (arrayList.size()-1) && y == (arrayList.get(z).getPoints().size()-1)) {
                    last = point;
                }
            }
            Log.d("distance: ", arrayList.get(z).getDistanceText());
            for (int j = 0; j < arrayList.get(z).getSegments().size(); j++) {
                Log.d("getInstruction: ", arrayList.get(z).getSegments().get(j).getInstruction());
                Log.d("getDistance: ", arrayList.get(z).getSegments().get(j).getDistance() + "");
                Log.d("getLength: ", arrayList.get(z).getSegments().get(j).getLength() + "");
            }
            Log.d("getDurationText: ", arrayList.get(z).getDurationText());
        }

        LatLng end = last;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(end);
        markerOptions.title("Arrivée");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        Travel travel = new Travel();
        travel.setFavorite(false);
        travel.setWork(false);
        travel.setHome(false);
        travel.setBeginDate(new Date());
        travel.setCreateDate(new Date());
        travel.setEndDate(new Date());

        travel.setFrom(emplacementInitiale);
        travel.setFromPos(positionInitiale);

        travel.setTo(emplacementFinale);
        travel.setToPos(positionFinale);

        ArrayList<TravelStep> itineraireRatp = new ArrayList<TravelStep>();

        travel.setItineraireRatp(itineraireRatp);

        onGoogleApiDirectionListener.onGoogleDirectionSuccess(options, markerOptions, travel);
    }
    */



}
