package com.example.alex.letsgoratp.fragment;

import com.example.alex.letsgoratp.model.Menu;

/**
 * Created by alex on 17/04/17.
 */

public interface IOnMenuSelected {
    public void onMenuSelected(Menu menu);
}
