package com.example.alex.letsgoratp.model;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.fetcher.DirectionHelper;
import com.squareup.okhttp.internal.Platform;

import org.w3c.dom.Text;

/**
 * Created by alex on 18/04/17.
 */

public class TravelAdapter  extends BaseAdapter {
    private Context context;
    private Travel travel;

    public TravelAdapter(Context context, Travel travel) {
        this.context = context;
        this.travel = travel;
    }

    @Override
    public int getCount() {
        return travel.getRoute().legs[0].steps.length;
    }

    @Override
    public Object getItem(int position) {
        return travel.getRoute().legs[0].steps[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.search_row, null);
        TextView modeTransport = (TextView) rowView.findViewById(R.id.modeTransport);

        TextView modeDirection = (TextView) rowView.findViewById(R.id.modeDirection);
        TextView modeDepart = (TextView) rowView.findViewById(R.id.modeDepart);
        TextView modeArret = (TextView) rowView.findViewById(R.id.modeArret);

        TextView distance = (TextView) rowView.findViewById(R.id.distance);
        TextView duration = (TextView) rowView.findViewById(R.id.duration);
        TextView addresse = (TextView) rowView.findViewById(R.id.addresse);
        TextView heureDebut = (TextView) rowView.findViewById(R.id.heureDebut);
        TextView heureArret = (TextView) rowView.findViewById(R.id.heureArret);

        View barreVerticale = (View) rowView.findViewById(R.id.barreVerticale);

        heureDebut.setText("10:00");
        duration.setText(travel.getRoute().legs[0].steps[position].duration.humanReadable.toString());
        distance.setText(travel.getRoute().legs[0].steps[position].distance.humanReadable.toString());
        addresse.setText(travel.getRoute().legs[0].steps[position].htmlInstructions.toString() + "");
        ImageView modeTransportImg = (ImageView) rowView.findViewById(R.id.modeTransportImg);
        Log.d("addresse: ",travel.getRoute().legs[0].steps[position].htmlInstructions.toString() );
        Log.d("travelMode: ",travel.getRoute().legs[0].steps[position].travelMode.toString() );

        if (travel.getRoute().legs[0].steps[position].travelMode.name().toString().toLowerCase().equals("walking")) {
            modeTransport.setText("A pied");
            heureDebut.setText("");
            modeTransportImg.setImageResource(R.mipmap.apied);
            barreVerticale.setBackgroundColor(Color.BLACK);
            modeDirection.setText("");
            modeDepart.setText("");
            modeArret.setText("");
            heureArret.setText("");
        } else {
            heureDebut.setText(travel.getRoute().legs[0].steps[position].transitDetails.departureTime.toString("HH:mm"));
            heureArret.setText(travel.getRoute().legs[0].steps[position].transitDetails.arrivalTime.toString("HH:mm"));
            DownloadImageTask imagesTask= new DownloadImageTask(modeTransportImg);
            modeTransport.setText(travel.getRoute().legs[0].steps[position].transitDetails.line.vehicle.name.toString());

            if (modeTransport.getText().equals("Métro")) {
                imagesTask.execute("https:" + travel.getRoute().legs[0].steps[position].transitDetails.line.vehicle.localIcon.toString());
            } else {
                imagesTask.execute("https:" + travel.getRoute().legs[0].steps[position].transitDetails.line.vehicle.icon.toString());

            }
            barreVerticale.setBackgroundColor(Color.parseColor(travel.getRoute().legs[0].steps[position].transitDetails.line.color.toString()));

            modeDirection.setText("Direction: " + travel.getRoute().legs[0].steps[position].transitDetails.headsign.toString());
            modeDepart.setText("Départ: " + travel.getRoute().legs[0].steps[position].transitDetails.departureStop.name.toString());
            modeArret.setText("Arrêt: " + travel.getRoute().legs[0].steps[position].transitDetails.arrivalStop.name.toString());
        }

        return rowView;
    }
}
