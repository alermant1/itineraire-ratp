package com.example.alex.letsgoratp.fetcher;

import com.example.alex.letsgoratp.model.Travel;
import com.google.maps.model.DirectionsRoute;

/**
 * Created by alex on 17/04/17.
 */

public interface IGoogleApiDirectionFetcher {
    public void onRoutingSuccess(DirectionsRoute route);
    public void onRoutingFailed(String erreur);
}
