package com.example.alex.letsgoratp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.model.FavoriteAdapter;
import com.example.alex.letsgoratp.repository.TravelRepository;

/**
 * Created by alex on 17/04/17.
 */

public class FavoriteFragment extends Fragment implements IGetMainLayout{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(getMainLayout(), null);
        ListView listViewFavorite = (ListView) v.findViewById(R.id.listViewFavorite);

        FavoriteAdapter adapter = new FavoriteAdapter(
                getContext(),
                TravelRepository.getInstance(getActivity()).getAllFavori()
        );

        listViewFavorite.setAdapter(adapter);
        return v;
    }

    @Override
    public int getMainLayout() {
        return R.layout.favorite_main;
    }
}