package com.example.alex.letsgoratp.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.alex.letsgoratp.model.Travel;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by xps on 18/04/17.
 */

public class TravelRepository implements ITravelRepository{

    private static TravelRepository instance;
    private DatabaseManager dbm;

    private TravelRepository(Context context){
        dbm = DatabaseManager.getInstance(context);
    }

    public static TravelRepository getInstance(Context context){
        if(instance==null){
            instance= new TravelRepository(context);
        }
        return instance;
    }
    @Override
    public boolean add(Travel t) {
        ContentValues values = new ContentValues();
        values.put("from_column", t.getFrom());
        values.put("to_column", t.getTo());
        values.put("toPos_column", ""+t.getToPos().latitude+";"+t.getToPos().longitude);
        values.put("fromPos_column", ""+t.getFromPos().latitude+";"+t.getFromPos().longitude);
        values.put("createDate_column", converteDateToString(t.getCreateDate()));
        values.put("beginDate_column", converteDateToString(t.getBeginDate()));
        values.put("endDate_column", converteDateToString(t.getEndDate()));
        values.put("isFavorite_column", t.isFavorite());
        values.put("isHome_column", t.isHome());
        values.put("isWork_column", t.isWork());
        values.put("isPlanned_column", t.isPlanned());
        values.put("markerFinale_column", ""+t.getMarkerFinale().getPosition().latitude+";"+t.getMarkerFinale().getPosition().longitude);
        long line= dbm.getWritableDatabase().insert("travel", null, values);
        return line != 0;
    }

    @Override
    public boolean addHome(Travel travel) {
        if ( travel.getId() == null ){
            removeOldHome();
            travel.setHome(true);
            return add(travel);
        }
        ContentValues values = new ContentValues();
        values.put("from_column", travel.getFrom());
        values.put("to_column", travel.getTo());
        values.put("toPos_column", ""+travel.getToPos().latitude+";"+travel.getToPos().longitude);
        values.put("fromPos_column", ""+travel.getFromPos().latitude+";"+travel.getFromPos().longitude);
        values.put("createDate_column", converteDateToString(travel.getCreateDate()));
        values.put("beginDate_column", converteDateToString(travel.getBeginDate()));
        values.put("endDate_column", converteDateToString(travel.getEndDate()));
        values.put("isFavorite_column", travel.isFavorite());
        values.put("isHome_column", true);
        values.put("isWork_column", travel.isWork());
        values.put("isPlanned_column", travel.isPlanned());
        values.put("markerFinale_column", ""+travel.getMarkerFinale().getPosition().latitude+";"+travel.getMarkerFinale().getPosition().longitude);
        String[] args = new String[]{travel.getId().toString()};
        long line= dbm.getWritableDatabase().update("travel", values, " WHERE id_column = ? ", args );
        return line != 0;

    }

    public boolean addWork(Travel travel) {
        if ( travel.getId() == null ){
            removeOldWork();
            travel.setWork(true);
            return add(travel);
        }
        ContentValues values = new ContentValues();
        values.put("from_column", travel.getFrom());
        values.put("to_column", travel.getTo());
        values.put("toPos_column", ""+travel.getToPos().latitude+";"+travel.getToPos().longitude);
        values.put("fromPos_column", ""+travel.getFromPos().latitude+";"+travel.getFromPos().longitude);
        values.put("createDate_column", converteDateToString(travel.getCreateDate()));
        values.put("beginDate_column", converteDateToString(travel.getBeginDate()));
        values.put("endDate_column", converteDateToString(travel.getEndDate()));
        values.put("isFavorite_column", travel.isFavorite());
        values.put("isHome_column", travel.isHome());
        values.put("isWork_column", travel.isWork());
        values.put("isPlanned_column", travel.isPlanned());
        values.put("markerFinale_column", ""+travel.getMarkerFinale().getPosition().latitude+";"+travel.getMarkerFinale().getPosition().longitude);
        String[] args = new String[]{travel.getId().toString()};
        long line= dbm.getWritableDatabase().update("travel", values, " id_column = ? ", args );
        return line != 0;

    }

    @Override
    public boolean removeOldHome() {
        ContentValues values = new ContentValues();
        values.put("isHome_column", "0");
        long line= dbm.getWritableDatabase().update("travel",  values, " isHome_column = 1 ", null );
        return line != 0;
    }

    @Override
    public boolean removeOldWork() {
        ContentValues values = new ContentValues();
        values.put("isWork_column", "0");
        long line= dbm.getWritableDatabase().update("travel",  values, " isWork_column = 1 ", null );
        return line != 0;
    }

    @Override
    public Travel getHome() {
        Travel travel = new Travel();
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel  where  isHome_column = 1", null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            c.moveToNext();
        }
        c.close();
        return travel;
    }

    @Override
    public Travel getTravel(Integer id) {
        Travel travel = new Travel();
        String[] args = new String[]{id.toString()};
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel  where  id_column = ?", args);
        c.moveToFirst();
        while (!c.isAfterLast()){
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            c.moveToNext();
        }
        c.close();
        return travel;
    }


    @Override
    public Travel getWork() {
        Travel travel = new Travel();
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel  where  isWork_column = 1", null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            c.moveToNext();
        }
        c.close();
        return travel;
    }

    @Override
    public boolean remove(Travel t) {
        String[] identifier = {t.getId().toString() };
        long line= dbm.getWritableDatabase().delete("travel", "id_column = ?", identifier);
        return line != 0;
    }





    @Override
    public ArrayList<Travel> getAll() {
        ArrayList<Travel> travels = new ArrayList<Travel>();
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel  ", null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            Travel travel = new Travel();
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            travels.add(travel);
            c.moveToNext();
        }
        c.close();
        return travels;

    }

    public ArrayList<Travel> getAllFavori() {
        ArrayList<Travel> travels = new ArrayList<Travel>();
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel where isFavorite_column = 1", null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            Travel travel = new Travel();
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            travels.add(travel);
            c.moveToNext();
        }
        c.close();
        return travels;

    }

    @Override
    public ArrayList<Travel> getAllPlanification() {
        ArrayList<Travel> travels = new ArrayList<Travel>();
        Cursor c= dbm.getReadableDatabase().rawQuery("select * from travel where isPlanned_column = 1  ORDER BY beginDate_column ASC ", null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            Travel travel = new Travel();
            travel.setId(Integer.parseInt(c.getString(0)));
            travel.setFrom(c.getString(1));
            travel.setTo(c.getString(2));
            travel.setToPos(convertLatLng(c.getString(3)));
            travel.setFromPos(convertLatLng(c.getString(4)));
            travel.setCreateDate(convertStringToDate(c.getString(5)));
            travel.setBeginDate(convertStringToDate(c.getString(6)));
            travel.setEndDate(convertStringToDate(c.getString(7)));
            travel.setFavorite( c.getString(8).equals("1") );
            travel.setHome(c.getString(9).equals("1"));
            travel.setWork(c.getString(10).equals("1"));
            travel.setPlanned(c.getString(11).equals("1"));
            travel.setMarkerFinale(getMarkPoint(c.getString(12)));
            travels.add(travel);
            c.moveToNext();
        }
        c.close();
        return travels;
    }

    public MarkerOptions getMarkPoint (String position){
        LatLng latLng = convertLatLng(position);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Moi");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        return markerOptions;
    }

    public LatLng convertLatLng(String position){
        String[] posi  =  position.split(";");
        double latitude = Double.parseDouble(posi[0]);
        double longitude = Double.parseDouble(posi[1]);
        LatLng postionF = new LatLng(latitude, longitude);

        return postionF;

    }

    public String converteDateToString(Date date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        return simpleDateFormat.format(date);

    }

    public static Date convertStringToDate(String date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }


    }

}
