package com.example.alex.letsgoratp.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.fetcher.DirectionHelper;
import com.example.alex.letsgoratp.fetcher.GoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IGoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IOnGoogleApiDirection;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.model.TravelAdapter;
import com.example.alex.letsgoratp.repository.TravelRepository;
import com.google.maps.model.DirectionsRoute;

/**
 * Created by alex on 17/04/17.
 */

public class HomeFragment extends Fragment implements IOnGoogleApiDirection, IGetMainLayout{
    private TravelRepository travelRepository;
    private Button letsgo;
    private ListView listViewInformations;
    private Travel home;
    MenuActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(getMainLayout(), null);
        listViewInformations = (ListView) v.findViewById(R.id.listViewInformations);
        travelRepository = TravelRepository.getInstance(getActivity());
        letsgo = (Button) v.findViewById(R.id.letsgo);
        activity = (MenuActivity) getActivity();

        letsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onGoogleDirectionSuccess(home);
            }
        });

        if(travelRepository.getHome().getId() != null){
            home = travelRepository.getHome();
            GoogleApiDirectionFetcher googleApi = new GoogleApiDirectionFetcher(getContext());
            googleApi.setOnGoogleApiDirectionListener(this);
            googleApi.init(home.getFrom(), home.getTo());
            googleApi.execute();
            letsgo.setVisibility(View.VISIBLE);
        }


        Log.d("onCreateView: ", "On passe ici");

        return v;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if(!hidden && travelRepository.getHome().getId() != null){
            activity.getDepart().setText(home.getFrom());
            activity.getDestination().setText(home.getTo());
        }

    }

    @Override
    public int getMainLayout() {
        return R.layout.home_main;
    }

    @Override
    public void onGoogleDirectionSuccess(Travel travel) {

        TravelAdapter adapter = new TravelAdapter(getContext(), travel);
        listViewInformations.setAdapter(adapter);
        home = travel;
    }

    @Override
    public void onGoogleDirectionFailed(String erreur) {

    }
}
