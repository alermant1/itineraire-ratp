package com.example.alex.letsgoratp.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by xps on 18/04/17.
 */


public  class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME= "mydb.sqlite";
    private static final int CURRENT_DB_VERSION = 1;

    private static DatabaseManager instance;

    public static DatabaseManager getInstance(Context context){
        if(instance==null){
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    private DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, CURRENT_DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table travel "+
                "(id_column INTEGER PRIMARY KEY  AUTOINCREMENT, from_column TEXT, to_column TEXT, toPos_column TEXT, fromPos_column TEXT, "+
                "createDate_column DATETIME, beginDate_column DATETIME, endDate_column DATETIME, " +
                "isFavorite_column BOOLEAN, isHome_column BOOLEAN, isWork_column BOOLEAN, isPlanned_column BOOLEAN,markerFinale_column TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        switch (i){
            case 1:
                //code sql 1->2
            case 2:
                //code sql 2->3
            case 3:
                //code sql 3->4
            default:
        }
    }
}


