package com.example.alex.letsgoratp.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by alex on 18/04/17.
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView myImageView;

    public DownloadImageTask(ImageView myImageView) {
        this.myImageView = myImageView;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadImage(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        myImageView.setImageBitmap(bitmap);
        myImageView.invalidate();
    }

    private Bitmap downloadImage(String url){
        Bitmap bm = null;
        try {
            URL aUrl = new URL(url);
            URLConnection conn = aUrl.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("Erreur", "Error getting image : "+ e.getMessage().toString());
        }
        return bm;
    }
}
