package com.example.alex.letsgoratp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.anshul.gooey.GooeyMenu;
import com.example.alex.letsgoratp.fetcher.DirectionHelper;
import com.example.alex.letsgoratp.fetcher.GoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IOnGoogleApiDirection;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.model.TravelAdapter;
import com.example.alex.letsgoratp.repository.TravelRepository;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Calendar;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, IOnGoogleApiDirection, GooeyMenu.GooeyMenuInterface {

    private final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleMap mMap;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private ArrayList<MarkerOptions> markers;
    private MapsActivity context;
    private GoogleApiClient mGoogleApiClient;
    private Travel travel;
    private LocationRequest mLocationRequest;
    private Polyline savedPolyline;
    private ListView listViewInformations;
    private ImageButton cacherButton;
    private ImageButton home;
    private ImageButton work;
    private ImageButton favori;
    private ImageButton planned;
    private TravelRepository travelRepository;
    private Boolean isFavori ;
    private Boolean isPlanned ;

    private RelativeLayout panelRelativeLayout;
    private RelativeLayout panelAddPlanned;

    private GooeyMenu mGooeyMenu;
    private Dialog dateDialog;
    private String date;
    private Dialog dialogTime;
    private DatePicker datepicker;
    private TimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        context = this;
        isFavori = false;
        isPlanned = false;
        cacherButton = (ImageButton) findViewById(R.id.cacher);
        home = (ImageButton) findViewById(R.id.home);
        work = (ImageButton) findViewById(R.id.work);
        favori = (ImageButton) findViewById(R.id.favori);
        planned = (ImageButton) findViewById(R.id.planned);



        mGooeyMenu = (GooeyMenu) findViewById(R.id.gooey_menu);
        mGooeyMenu.setOnMenuListener(this);



        listViewInformations = (ListView) findViewById(R.id.listViewInformations);

        panelRelativeLayout = (RelativeLayout) findViewById(R.id.panel);
        panelAddPlanned = (RelativeLayout) findViewById(R.id.addPlanned);

        panelRelativeLayout.setVisibility(View.GONE);
        panelAddPlanned.setVisibility(View.GONE);

        // permet de fermer la liste du trajet


        buttonAction();

        // on enregistre les markers affichés sur la carte
        markers = new ArrayList<MarkerOptions>();
    }

    public void buttonAction(){

        cacherButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                panelRelativeLayout.setVisibility(View.GONE);
                Log.d("listViewInformations", "On cache");

            }
        });

        travelRepository = TravelRepository.getInstance(this);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travel != null) {
                    travelRepository.addHome(travel);
                    alertMessage("Votre trajet a été assigné comme itineraire Maison !", "Félicitation");

                }
            }
        });

        travelRepository = TravelRepository.getInstance(this);
        work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travel != null) {
                    travelRepository.addWork(travel);
                    alertMessage("Votre trajet a été assigné comme itineraire Travail !", "Félicitation");

                }
            }
        });

        travelRepository = TravelRepository.getInstance(this);
        favori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travel != null && !isFavori) {
                    travel.setFavorite(true);
                    travelRepository.add(travel);
                    isFavori = true;
                    alertMessage("Votre trajet a été ajouté à vos favoris !", "Félicitation");
                }
            }
        });

        travelRepository = TravelRepository.getInstance(this);
        planned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (travel != null && !isPlanned) {
                    dateDialog = new Dialog(context);

                    dateDialog.setContentView(R.layout.date_dialog);
                    dateDialog.setTitle("Planification - Jour");
                    dateDialog.show();
                    ImageButton valideDate = (ImageButton) dateDialog.findViewById(R.id.valideDate);
                    datepicker = (DatePicker) dateDialog.findViewById(R.id.datePicker1);

                    valideDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            date = datepicker.getYear() + "/" + datepicker.getMonth() + "/" + datepicker.getDayOfMonth();
                            dateDialog.dismiss();
                            dialogTime.show();
                        }
                    });
                    
                    dialogTime = new Dialog(context);
                    dialogTime.setContentView(R.layout.time_dialog);
                    dialogTime.setTitle("Planification - Heures");
                    timePicker = (TimePicker) dialogTime.findViewById(R.id.timePicker1);

                    ImageButton valideTime = (ImageButton) dialogTime.findViewById(R.id.valideTime);
                    valideTime.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            date += " " + timePicker.getHour() + ":" + timePicker.getMinute();
                            dialogTime.dismiss();
                            travel.setBeginDate(TravelRepository.convertStringToDate(date));
                            travel.setPlanned(true);
                            alertMessage("Votre parcours du " + date + " a été enregistré.","Enregistrement effectué !");
                            travelRepository.add(travel);
                        }
                    });


                }
            }
        });
    }


    public void alertMessage(String message, String title){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create()
                .show();
    }

    public void startA() {
        Intent intent = new Intent(this, MenuActivity.class);

        if (mLastLocation != null) {
            intent.putExtra("lastKnowLocationLatitude", mLastLocation.getLatitude() + "");
            intent.putExtra("lastKnowLocationLongitude", mLastLocation.getLongitude() + "");
        } else {
            intent.putExtra("lastKnowLocationLatitude", "");
            intent.putExtra("lastKnowLocationLongitude", "");
        }

        startActivityForResult(intent, 0);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {

                // Affiche un po
                new AlertDialog.Builder(this)
                    .setTitle("Demande de permission")
                    .setMessage("L'application a besoin de connaitre votre position et d'accéder à vos contacts pour les rejoindre.")
                    .setPositiveButton("Autoriser", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(MapsActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    })
                    .create()
                    .show();

            } else {
                // we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
            markers.remove(mCurrLocationMarker);
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Moi");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        markers.add(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check that it is the SecondActivity with an OK result
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                // get String data from Intent
                this.travel = (Travel) data.getExtras().getParcelable("Travel");
                GoogleApiDirectionFetcher GoogleApiDirectionFetcher = new GoogleApiDirectionFetcher(this);
                GoogleApiDirectionFetcher.setOnGoogleApiDirectionListener(this);
                GoogleApiDirectionFetcher.init(travel.getFrom(), travel.getTo());
                GoogleApiDirectionFetcher.execute();
            }
        }
    }

    public void onTravelSelected(Travel travelSelected) {
        if(travelSelected == null || travelSelected.getFromPos() == null){
            return;
        }
        Location locPos = new Location("dummyprovider");
        locPos.setLatitude(travelSelected.getFromPos().latitude);
        locPos.setLongitude(travelSelected.getFromPos().longitude);

        onLocationChanged(locPos);
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);

        for (com.google.maps.model.LatLng pos: travelSelected.getRoute().overviewPolyline.decodePath()) {
            LatLng latlng = new LatLng(pos.lat, pos.lng);
            Log.d(pos.lat + "", pos.lng + "");
            options.add(latlng);
        }

        // si on avait précédement déja le parcours, on l'efface
        if (savedPolyline != null) {
            savedPolyline.remove();
        }
        savedPolyline = mMap.addPolyline(options);
        mMap.addMarker(travelSelected.getMarkerFinale());
        markers.add(travelSelected.getMarkerFinale());

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLng pos = new LatLng(travelSelected.getRoute().bounds.northeast.lat, travelSelected.getRoute().bounds.northeast.lng);
        builder.include(pos);
        pos = new LatLng(travelSelected.getRoute().bounds.southwest.lat, travelSelected.getRoute().bounds.southwest.lng);
        builder.include(pos);
        LatLngBounds bounds = builder.build();

        int padding = 30; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);

        TravelAdapter adapter = new TravelAdapter(this, travelSelected);
        listViewInformations.setAdapter(adapter);
        isFavori = false;
        isPlanned = false;
        travel = travelSelected;
    }

    @Override
    public void onGoogleDirectionSuccess(Travel Travel) {
        onTravelSelected(Travel);
    }

    @Override
    public void onGoogleDirectionFailed(String erreur) {
        Log.d("onGoogleDirectionFailed", "Erreur");
    }

    @Override
    public void menuOpen() {
        
    }

    @Override
    public void menuClose() {

    }

    @Override
    public void menuItemClicked(int i) {
        Log.d("on clique sur", i + "");

        switch (i){
            case 1:
                startA();
                break;
            case 2:
                Travel travel = travelRepository.getHome();
                GoogleApiDirectionFetcher GoogleApiDirectionFetcher = new GoogleApiDirectionFetcher(this);
                GoogleApiDirectionFetcher.setOnGoogleApiDirectionListener(this);
                GoogleApiDirectionFetcher.init(travel.getFrom(), travel.getTo());
                GoogleApiDirectionFetcher.execute();
                break;
            case 3:
                Travel travelW = travelRepository.getWork();
                GoogleApiDirectionFetcher GoogleApiDirectionFetcherW = new GoogleApiDirectionFetcher(this);
                GoogleApiDirectionFetcherW.setOnGoogleApiDirectionListener(this);
                GoogleApiDirectionFetcherW.init(travelW.getFrom(), travelW.getTo());
                GoogleApiDirectionFetcherW.execute();
                break;
            case 4:
                panelRelativeLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }
}
