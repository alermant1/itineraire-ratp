package com.example.alex.letsgoratp.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alex.letsgoratp.R;

import java.util.ArrayList;

/**
 * Created by alex on 17/04/17.
 */
public class MenuAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Menu> menus;

    public MenuAdapter(Activity context, ArrayList<Menu> menus){
        super();
        this.context= context;
        this.menus= menus;
    }

    @Override
    public int getCount() {
        return menus.size();
    }

    @Override
    public Object getItem(int position) {
        return menus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.menu_row, null);
        TextView textMenu = (TextView) rowView.findViewById(R.id.textMenu);
        //ImageView imgMenu = (ImageView) rowView.findViewById(R.id.imgMenu);
        textMenu.setText(menus.get(position).getName());
        //imgMenu.setImageResource(context.getResources().getIdentifier("drawable/" + menus.get(position).getIcon(), null, context.getPackageName()));
        return rowView;
    }
}