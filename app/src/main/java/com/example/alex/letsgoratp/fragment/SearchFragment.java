package com.example.alex.letsgoratp.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.fetcher.DirectionHelper;
import com.example.alex.letsgoratp.fetcher.GoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IOnGoogleApiDirection;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.model.TravelAdapter;
import com.example.alex.letsgoratp.repository.DatabaseManager;
import com.example.alex.letsgoratp.repository.TravelRepository;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by alex on 17/04/17.
 */

public class SearchFragment extends Fragment implements IGetMainLayout, IOnGoogleApiDirection{
    String destination;
    String depart;
    private ListView listViewMenu;
    private View v;
    private Travel selectedTravel;
    private TravelRepository  travelRepository;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(getMainLayout(), null);
        Button letsgo = (Button) v.findViewById(R.id.letsgo);
        letsgo.setVisibility(View.VISIBLE);
        letsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedTravel != null) {
                    sendTravel();
                }
            }
        });
        return v;
    }

    @Override
    public int getMainLayout() {
        return R.layout.search_result;
    }

    public void sendTravel() {
        travelRepository = TravelRepository.getInstance(getActivity());
        travelRepository.add(selectedTravel);
        Log.d("sendTravel", "on envoit le travel au MenuActivity");
        MenuActivity activity = (MenuActivity) getActivity();
        activity.onGoogleDirectionSuccess(selectedTravel);
    }
    public void init() {
        GoogleApiDirectionFetcher GoogleApiDirectionFetcher = new GoogleApiDirectionFetcher(getContext());
        GoogleApiDirectionFetcher.setOnGoogleApiDirectionListener(this);
        GoogleApiDirectionFetcher.init(depart, destination);
        GoogleApiDirectionFetcher.execute();
    }

    @Override
    public void onGoogleDirectionSuccess(Travel Travel) {
        Log.d("Success: ", Travel.getRoute().legs[0].endAddress);
        // on charge la listeView dans le layout  R.layout.search_result
        listViewMenu = (ListView) v.findViewById(R.id.listViewMenu);
        TravelAdapter adapter = new TravelAdapter(getContext(), Travel);
        listViewMenu.setAdapter(adapter);
        ((BaseAdapter)listViewMenu.getAdapter()).notifyDataSetChanged();
        selectedTravel = Travel;
    }

    @Override
    public void onGoogleDirectionFailed(String erreur) {
        new AlertDialog.Builder(getContext())
            .setTitle(erreur)
            .setMessage(erreur)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }

}
