package com.example.alex.letsgoratp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.fetcher.GoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IOnGoogleApiDirection;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.model.TravelAdapter;
import com.example.alex.letsgoratp.repository.TravelRepository;

/**
 * Created by alex on 17/04/17.
 */

public class WorkFragment extends Fragment implements IOnGoogleApiDirection, IGetMainLayout{
    private TravelRepository travelRepository;
    private Button letsgo;
    private ListView listViewInformations;
    private Travel work;
    MenuActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(getMainLayout(), null);
        listViewInformations = (ListView) v.findViewById(R.id.listViewInformations);
        travelRepository = TravelRepository.getInstance(getActivity());
        letsgo = (Button) v.findViewById(R.id.letsgo);
        activity = (MenuActivity) getActivity();

        letsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onGoogleDirectionSuccess(work);
            }
        });

        if(travelRepository.getWork().getId() != null){
            work = travelRepository.getWork();
            GoogleApiDirectionFetcher googleApi = new GoogleApiDirectionFetcher(getContext());
            googleApi.setOnGoogleApiDirectionListener(this);
            googleApi.init(work.getFrom(), work.getTo());
            googleApi.execute();
            letsgo.setVisibility(View.VISIBLE);
        }

        return v;
    }

    @Override
    public int getMainLayout() {
        return R.layout.work_main;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if(!hidden  &&  travelRepository.getWork().getId() != null){
            activity.getDepart().setText(work.getFrom());
            activity.getDestination().setText(work.getTo());
        }

    }


    @Override
    public void onGoogleDirectionSuccess(Travel Travel) {
        Log.d("onGoogleDirectionSuccess: ", "on passe ici apres le wok");
        TravelAdapter adapter = new TravelAdapter(getContext(), Travel);
        listViewInformations.setAdapter(adapter);
        work = Travel;
    }

    @Override
    public void onGoogleDirectionFailed(String erreur) {

    }
}
