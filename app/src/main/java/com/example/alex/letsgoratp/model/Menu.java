package com.example.alex.letsgoratp.model;

import android.app.Fragment;

/**
 * Created by alex on 17/04/17.
 */

public class Menu {
    private String name;
    private Fragment fragment;
    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Menu(String name, Fragment fragment, String icon) {
        this.name = name;
        this.fragment = fragment;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }


}
