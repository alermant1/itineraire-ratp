package com.example.alex.letsgoratp.fetcher;

import com.example.alex.letsgoratp.model.Travel;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by alex on 17/04/17.
 */

public interface IOnGoogleApiDirection {
    public void onGoogleDirectionSuccess(Travel Travel);
    public void onGoogleDirectionFailed(String erreur);
}
