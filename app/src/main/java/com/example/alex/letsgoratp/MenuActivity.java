package com.example.alex.letsgoratp;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.alex.letsgoratp.fetcher.DirectionHelper;
import com.example.alex.letsgoratp.fetcher.GoogleApiDirectionFetcher;
import com.example.alex.letsgoratp.fetcher.IOnGoogleApiDirection;
import com.example.alex.letsgoratp.fragment.IOnMenuSelected;
import com.example.alex.letsgoratp.fragment.MenuFragment;
import com.example.alex.letsgoratp.fragment.SearchFragment;
import com.example.alex.letsgoratp.model.Menu;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.repository.DatabaseManager;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by alex on 17/04/17.
 */

public class MenuActivity extends AppCompatActivity implements IOnGoogleApiDirection, IOnMenuSelected{


    private SearchFragment SearchFragment;
    private Fragment selectedFragment;
    private ImageButton buttonSearch;
    private MenuFragment MenuFragment;
    private LatLng lastKnowLocation;
    GoogleApiDirectionFetcher GoogleApiDirectionFetcher;
    private EditText depart;
    private EditText destination;

    public EditText getDepart() {
        return depart;
    }

    public void setDepart(EditText depart) {
        this.depart = depart;
    }

    public EditText getDestination() {
        return destination;
    }

    public void setDestination(EditText destination) {
        this.destination = destination;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);
        depart = (EditText) findViewById(R.id.depart);
        destination = (EditText) findViewById(R.id.destination);

        Bundle bundle = getIntent().getExtras();

        lastKnowLocation = null;
        String lastKnowLocationLatitude = bundle.getString("lastKnowLocationLatitude");
        String lastKnowLocationLongitude = bundle.getString("lastKnowLocationLongitude");

        if (!lastKnowLocationLatitude.equals("") && !lastKnowLocationLongitude.equals("")) {
            lastKnowLocation = new LatLng(
                    Double.parseDouble(lastKnowLocationLatitude),
                    Double.parseDouble(lastKnowLocationLongitude)
            );
            depart.setText(DirectionHelper.getAddressFromLocation(lastKnowLocation.latitude, lastKnowLocation.longitude, this));
        }

        GoogleApiDirectionFetcher = new GoogleApiDirectionFetcher(this);
        GoogleApiDirectionFetcher.setOnGoogleApiDirectionListener(this);
        // GoogleApiDirectionFetcher.init("2 rue Roland Oudot Créteil", "Jussieu Paris");
        // GoogleApiDirectionFetcher.execute();

        MenuFragment = new MenuFragment();
        SearchFragment = new SearchFragment();
        this.setTitle("Menu");

        // on charge les fragments
        chargerFragments();

        // on affiche le menu
        selectedFragment = MenuFragment;
        afficherFragment(MenuFragment);

        MenuFragment.setOnMenuSelectedListener(this);

        buttonSearch = (ImageButton) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (destination.getText().length() > 0 && depart.getText().length() > 0) {
                    SearchFragment.setDepart(depart.getText().toString());
                    SearchFragment.setDestination(destination.getText().toString());
                    SearchFragment.init();
                    afficherFragment(SearchFragment);
                    InputMethodManager imm = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(destination.getWindowToken(), 0);
                    imm.hideSoftInputFromWindow(depart.getWindowToken(), 0);
                }
            }
        });
    }

    public void afficherFragment(Fragment fragment)
    {
        getFragmentManager().beginTransaction()
                .hide(selectedFragment)
                .show(fragment)
                .commit();
    }

    public void chargerFragments()
    {
        getFragmentManager().beginTransaction()
                .add(R.id.frameLayout, SearchFragment)
                .add(R.id.frameLayout, MenuFragment)
                .hide(SearchFragment)
                .commit();
        for(int i = 0; i < MenuFragment.getMenus().size(); i++) {
            getFragmentManager().beginTransaction()
                    .add(R.id.frameLayout, MenuFragment.getMenus().get(i).getFragment())
                    .hide(MenuFragment.getMenus().get(i).getFragment())
                    .commit();
        }
    }

    @Override
    public void onGoogleDirectionSuccess(Travel travel) {

        Log.d("onGoogleDirectionSucces", "ok");

        Intent intent = new Intent();
        intent.putExtra("Travel", travel);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onGoogleDirectionFailed(String erreur) {
        Log.d("onGoogleDirectionFailed", "ok");
    }

    @Override
    public void onMenuSelected(Menu menu) {
        afficherFragment(menu.getFragment());
        selectedFragment = menu.getFragment();
        this.setTitle(menu.getName());
    }
}
