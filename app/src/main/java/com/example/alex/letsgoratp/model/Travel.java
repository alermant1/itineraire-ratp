package com.example.alex.letsgoratp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.model.DirectionsRoute;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by alex on 16/04/17.
 */

public class Travel implements Parcelable{
    private String from;
    private String to;
    private LatLng toPos;
    private LatLng fromPos;
    private Date createDate;
    private Date beginDate;
    private Date endDate;
    private boolean isFavorite;
    private boolean isHome;
    private boolean isWork;
    private boolean isPlanned;
    private MarkerOptions markerFinale;
    private DirectionsRoute route;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Travel() {

    }

    protected Travel(Parcel in) {
        from = in.readString();
        to = in.readString();
        toPos = in.readParcelable(LatLng.class.getClassLoader());
        fromPos = in.readParcelable(LatLng.class.getClassLoader());
        isFavorite = in.readByte() != 0;
        isHome = in.readByte() != 0;
        isWork = in.readByte() != 0;
        isPlanned = in.readByte() != 0;
        markerFinale = in.readParcelable(MarkerOptions.class.getClassLoader());
    }

    public static final Creator<Travel> CREATOR = new Creator<Travel>() {
        @Override
        public Travel createFromParcel(Parcel in) {
            return new Travel(in);
        }

        @Override
        public Travel[] newArray(int size) {
            return new Travel[size];
        }
    };

    public DirectionsRoute getRoute() {
        return route;
    }

    public void setRoute(DirectionsRoute route) {
        this.route = route;
    }

    public MarkerOptions getMarkerFinale() {
        return markerFinale;
    }

    public void setMarkerFinale(MarkerOptions markerFinale) {
        this.markerFinale = markerFinale;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public LatLng getToPos() {
        return toPos;
    }

    public void setToPos(LatLng toPos) {
        this.toPos = toPos;
    }

    public LatLng getFromPos() {
        return fromPos;
    }

    public void setFromPos(LatLng fromPos) {
        this.fromPos = fromPos;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isHome() {
        return isHome;
    }

    public void setHome(boolean home) {
        isHome = home;
    }

    public boolean isWork() {
        return isWork;
    }

    public void setWork(boolean work) {
        isWork = work;
    }

    public boolean isPlanned() { return isPlanned; }

    public void setPlanned(boolean planned) { isPlanned = planned; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(from);
        dest.writeString(to);
        dest.writeParcelable(toPos, flags);
        dest.writeParcelable(fromPos, flags);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
        dest.writeByte((byte) (isHome ? 1 : 0));
        dest.writeByte((byte) (isWork ? 1 : 0));
        dest.writeByte((byte) (isPlanned ? 1 : 0));
        dest.writeParcelable(markerFinale, flags);
    }
}
