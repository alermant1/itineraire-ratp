package com.example.alex.letsgoratp.repository;

import com.example.alex.letsgoratp.model.Travel;

import java.util.ArrayList;

/**
 * Created by xps on 18/04/17.
 */

public interface ITravelRepository {

    public boolean add(Travel travel);

    public boolean addHome(Travel travel);

    public boolean addWork(Travel travel);

    public boolean removeOldHome();

    public boolean removeOldWork();


    public Travel getHome();

    public Travel getTravel(Integer id);

    public Travel getWork();

    public boolean remove(Travel travel);

    public ArrayList<Travel> getAllFavori();

    public ArrayList<Travel> getAllPlanification();

    public ArrayList<Travel> getAll();
}
