package com.example.alex.letsgoratp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.model.Menu;
import com.example.alex.letsgoratp.model.MenuAdapter;

import java.util.ArrayList;

/**
 * Created by alex on 17/04/17.
 */

public class MenuFragment extends Fragment implements IGetMainLayout{
    private ArrayList<Menu> menus;
    private MenuAdapter adapter;

    ListView listViewMenu;
    private IOnMenuSelected onMenuSelectedListener;

    public void setOnMenuSelectedListener(IOnMenuSelected onMenuSelectedListener) {
        this.onMenuSelectedListener = onMenuSelectedListener;
    }

    public MenuFragment() {
        menus = new ArrayList<Menu>();
        menus.add(new Menu("Domicile", new HomeFragment(), "ic_search_48px"));
        menus.add(new Menu("Travail", new WorkFragment(), "ic_search_48px"));
        menus.add(new Menu("Favoris", new FavoriteFragment(), "ic_search_48px"));
        menus.add(new Menu("Trajets planifiés", new TravelFragment(), "ic_search_48px"));
        menus.add(new Menu("Rejoindre un contact", new ContactFragment(), "ic_search_48px"));
        menus.add(new Menu("Mes derniers trajets", new HistoryFragment(),"ic_search_48px"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v= inflater.inflate(getMainLayout(), null);

        listViewMenu = (ListView) v.findViewById(R.id.listViewMenu);
        adapter = new MenuAdapter(getActivity(), menus);

        listViewMenu.setAdapter(adapter);

        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onMenuSelectedListener.onMenuSelected(menus.get(position));
            }
        });

        return v;
    }

    public ArrayList<Menu> getMenus() {
        return menus;
    }
    @Override
    public int getMainLayout() {
        return R.layout.menu_main;
    }
}
