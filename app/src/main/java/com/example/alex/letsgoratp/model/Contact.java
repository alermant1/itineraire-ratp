package com.example.alex.letsgoratp.model;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

/**
 * Created by alex on 19/04/17.
 */

public class Contact {
    private String displayName;
    private String numeroTelephone;
    private String adresse;
    private int contactId;
    private Context context;

    public Contact(Context context) {
        this.displayName = "";
        this.numeroTelephone = "";
        this.adresse = "";
        this.contactId = 0;
        this.context = context;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getAdresse() {
        if(contactId == 0) {
            return "";
        }

        String postalData = "";
        String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
        String[] addrWhereParams = new String[] { String.valueOf(contactId), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE };

        Cursor addrCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, addrWhere, addrWhereParams, null);

        if (addrCur.moveToFirst()) {
            postalData = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));
        }
        addrCur.close();
        this.adresse = postalData;
        return postalData;
    }


    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
