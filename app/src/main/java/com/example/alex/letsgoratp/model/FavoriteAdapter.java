package com.example.alex.letsgoratp.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.repository.TravelRepository;

import java.util.ArrayList;

/**
 * Created by alex on 19/04/17.
 */

public class FavoriteAdapter extends BaseAdapter {


    private final Context context;
    private final ArrayList<Travel> travels;

    public FavoriteAdapter(Context context, ArrayList<Travel> travels) {
        this.context = context;
        this.travels = travels;
    }

    @Override
    public int getCount() {
        return travels.size();
    }

    @Override
    public Object getItem(int position) {
        return travels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.favorite_row, null);
        TextView emplacementAllee = (TextView) rowView.findViewById(R.id.emplacementAllee);
        TextView emplacementDestination = (TextView)rowView.findViewById(R.id.emplacementDestination);
        Button buttonLetsGo = (Button) rowView.findViewById(R.id.buttonLetsGo);

        buttonLetsGo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MenuActivity activity = (MenuActivity) context;
                activity.getDestination().setText(travels.get(position).getTo());
                activity.getDepart().setText(travels.get(position).getFrom());
            }
        });

        Button buttonSupprimer = (Button) rowView.findViewById(R.id.buttonSupprimer);
        buttonSupprimer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TravelRepository.getInstance(context).remove(travels.get(position));
                travels.get(position).setFavorite(false);
                TravelRepository.getInstance(context).add(travels.get(position));
                travels.remove(position);
                notifyDataSetChanged();

            }
        });
        emplacementAllee.setText(travels.get(position).getFrom());
        emplacementDestination.setText(travels.get(position).getTo());
        return rowView;
    }
}