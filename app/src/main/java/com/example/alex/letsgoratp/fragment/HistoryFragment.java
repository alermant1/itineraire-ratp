package com.example.alex.letsgoratp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.model.FavoriteAdapter;
import com.example.alex.letsgoratp.model.HistoryAdapter;
import com.example.alex.letsgoratp.model.Travel;
import com.example.alex.letsgoratp.repository.TravelRepository;

import java.util.ArrayList;

/**
 * Created by alex on 17/04/17.
 */

public class HistoryFragment  extends Fragment implements IGetMainLayout{
    private ArrayList<Travel> travels;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(getMainLayout(), null);
        ListView listViewHistory = (ListView) v.findViewById(R.id.listViewHistory);
        travels = TravelRepository.getInstance(getActivity()).getAll();

        HistoryAdapter adapter = new HistoryAdapter(
            getContext(), travels
        );
        listViewHistory.setAdapter(adapter);
        listViewHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuActivity activity = (MenuActivity) getContext();
                activity.getDestination().setText(travels.get(position).getTo());
                activity.getDepart().setText(travels.get(position).getFrom());
            }
        });

        return v;
    }

    @Override
    public int getMainLayout() {
        return R.layout.history_main;
    }
}
