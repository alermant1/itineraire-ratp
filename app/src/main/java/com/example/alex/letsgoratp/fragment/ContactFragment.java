package com.example.alex.letsgoratp.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.example.alex.letsgoratp.MapsActivity;
import com.example.alex.letsgoratp.MenuActivity;
import com.example.alex.letsgoratp.R;
import com.example.alex.letsgoratp.model.Contact;
import com.example.alex.letsgoratp.model.ContactAdapter;

import java.util.ArrayList;

/**
 * Created by alex on 17/04/17.
 */

public class ContactFragment  extends Fragment implements IGetMainLayout{

    private ArrayList<Contact> contacts;

    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(getMainLayout(), container, false);
        contacts = new ArrayList<Contact>();
        Cursor phones = getActivity().getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);

        while (phones.moveToNext())
        {
            String displayName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            int contactId = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));

            Contact unContact = new Contact(getActivity().getApplicationContext());
            unContact.setDisplayName(displayName);
            unContact.setNumeroTelephone(phoneNumber);
            unContact.setContactId(contactId);
            contacts.add(unContact);
        }

        phones.close();
        ContactAdapter adapter = new ContactAdapter(getContext(), contacts);
        ListView listViewContact = (ListView) v.findViewById(R.id.listViewContact);
        listViewContact.setAdapter(adapter);
        ((BaseAdapter)listViewContact.getAdapter()).notifyDataSetChanged();

        listViewContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuActivity activity = (MenuActivity) getActivity();
                activity.getDestination().setText(contacts.get(position).getAdresse());
            }
        });
        return v;
    }

    @Override
    public int getMainLayout() {
        return R.layout.contact_main;
    }


}
